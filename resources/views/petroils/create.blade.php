@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a petroil</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('petroils.store') }}">
          @csrf
          <div class="form-group">    
              <label for="type_oil">Type of oil:</label>
              <input type="string" class="form-control" name="type_oil"/>
          </div>

          <div class="form-group">
              <label for="percentage">Percentage:</label>
              <input type="integer" class="form-control" name="percentage"/>
          </div>

          <div class="form-group">
              <label for="cost_hour">Cost por hour:</label>
              <input type="integer" class="form-control" name="cost_hour"/>
          </div>                 
          <button type="submit" class="btn btn-primary-outline">Add petroil</button>
      </form>
  </div>
</div>
</div>
@endsection