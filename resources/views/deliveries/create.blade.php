@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a delivery</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('deliveries.store') }}">
          @csrf
          <div class="form-group">    
              <label for="name_customer">Name Customer:</label>
              <input type="string" class="form-control" name="name_customer"/>
          </div>

          <div class="form-group">
              <label for="dimension">Dimension:</label>
              <input type="integer" class="form-control" name="dimension"/>
          </div>

          <div class="form-group">
              <label for="type_oil">Type of oil:</label>
              <input type="string" class="form-control" name="type_oil"/>
          </div>
          <div class="form-group">
              <label for="oil_liter">Oil liter:</label>
              <input type="integer" class="form-control" name="oil_liter"/>
          </div>
          <div class="form-group">
              <label for="cost">Cost:</label>
              <input type="integer" class="form-control" name="cost"/>
          </div>
          <div class="form-group">
              <label for="address">Address :</label>
              <input type="string" class="form-control" name="address"/>
          </div>  
          <div class="form-group">
              <label for="phone_number">Phone number :</label>
              <input type="string" class="form-control" name="phone_number"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Add delivery</button>
      </form>
  </div>
</div>
</div>
@endsection