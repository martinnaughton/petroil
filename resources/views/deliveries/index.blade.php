@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Delivery</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('deliveries.create')}}" class="btn btn-primary">New delivery</a>
    </div>  
    <thead>
        <tr>
          <td>ID</td>
          <td>Name customer</td>
          <td>Dimension</td>
          <td>Type oil</td>
          <td>Oil liter</td>
          <td>Cost</td>
          <td>Address</td>
          <td>Phone number</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($deliveries as $delivery)
        <tr>
            <td>{{$delivery->id}}</td>
            <td>{{$delivery->name_customer}}</td>
            <td>{{$delivery->dimension}}</td>
            <td>{{$delivery->type_oil}}</td>
            <td>{{$delivery->oil_liter}}</td>
            <td>{{$delivery->cost}}</td>
            <td>{{$delivery->address}}</td>
            <td>{{$delivery->phone_number}}</td>
            <td>
                <a href="{{ route('deliveries.edit',$delivery->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('deliveries.destroy', $delivery->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection