@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a service</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('services.store') }}">
          @csrf
          <div class="form-group">    
              <label for="position">Position:</label>
              <input type="string" class="form-control" name="position"/>
          </div>

          <div class="form-group">
              <label for="data">Data:</label>
              <input type="date" class="form-control" name="data"/>
          </div>

          <div class="form-group">
              <label for="time">Time:</label>
              <input type="time" class="form-control" name="time"/>
          </div>
          <div class="form-group">
              <label for="product">Product:</label>
              <input type="string" class="form-control" name="product"/>
          </div>
          <div class="form-group">
              <label for="quantity">Quantity:</label>
              <input type="integer" class="form-control" name="quantity"/>
          </div>
          <div class="form-group">
              <label for="vender">Vender:</label>
              <input type="string" class="form-control" name="vender"/>
          </div>                         
          <button type="submit" class="btn btn-primary-outline">Add vender</button>
      </form>
  </div>
</div>
</div>
@endsection