@extends('base')

@section('main')
<div class="row">
 <div class="col-sm-8 offset-sm-2">
    <h1 class="display-3">Add a drum</h1>
  <div>
    @if ($errors->any())
      <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
            @endforeach
        </ul>
      </div><br />
    @endif
      <form method="post" action="{{ route('drums.store') }}">
          @csrf
          <div class="form-group">    
              <label for="dimension">Dimension:</label>
              <input type="integer" class="form-control" name="dimension"/>
          </div>

          <div class="form-group">
              <label for="max_liter">Max liter:</label>
              <input type="integer" class="form-control" name="max_liter"/>
          </div>

          <div class="form-group">
              <label for="position">Position:</label>
              <input type="string" class="form-control" name="position"/>
          </div>

         <!-- <p> {{$drums ?? ''}} </p> -->
          <button type="submit" class="btn btn-primary-outline">Add drum</button>
      </form>
  </div>
</div>
</div>
@endsection