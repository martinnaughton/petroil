@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Orders</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('orders.create')}}" class="btn btn-primary">New order</a>
    </div> 
    <thead>
        <tr>
          <td>ID</td>
          <td>Dimension</td>
          <td>Type of oil</td>
          <td>Oil liter</td>
          <td>Cost</td>
          <td>Delivery</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($orders as $order)
        <tr>
            <td>{{$order->id}}</td>
            <td>{{$order->dimension}}</td>
            <td>{{$order->type_oil}}</td>
            <td>{{$order->oil_liter}}</td>
            <td>{{$order->cost}}</td>
            <td>{{$order->delivery}}</td>
            <td>
                <a href="{{ route('orders.edit',$order->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('orders.destroy', $order->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection