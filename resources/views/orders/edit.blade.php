@extends('base') 
@section('main')
<div class="row">
    <div class="col-sm-8 offset-sm-2">
        <h1 class="display-3">Update a order</h1>

        @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
        <br /> 
        @endif
        <form method="post" action="{{ route('orders.update', $order->id) }}">
            @method('PATCH') 
            @csrf
            <div class="form-group">

                <label for="dimension">Dimension:</label>
                <input type="integer" class="form-control" name="dimension" value={{ $order->dimension }} />
            </div>

            <div class="form-group">
                <label for="type_oil">Type of oil:</label>
                <input type="string" class="form-control" name="type_oil" value={{ $order->type_oil }} />
            </div>

            <div class="form-group">
                <label for="oil_liter">Oil liter:</label>
                <input type="integer" class="form-control" name="oil_liter" value={{ $order->oil_liter }} />
            </div>
            <div class="form-group">
                <label for="cost">Cost:</label>
                <input type="integer" class="form-control" name="cost" value={{ $order->cost }} />
            </div>
            <div class="form-group">
                <label for="delivery">Delivery:</label>
                <input type="boolean" class="form-control" name="delivery" value={{ $order->delivery }} />
            </div>
            <button type="submit" class="btn btn-primary">Update</button>
        </form>
    </div>
</div>
@endsection