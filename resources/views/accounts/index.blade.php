@extends('base')

@section('main')
<div class="row">
<div class="col-sm-12">
    <h1 class="display-3">Accounts</h1>    
  <table class="table table-striped">
  <div>
    <a style="margin: 19px;" href="{{ route('accounts.create')}}" class="btn btn-primary">New account</a>
    </div>  
    <thead>
        <tr>
          <td>ID</td>
          <td>Name</td>
          <td>Surname</td>
          <td>Email</td>
          <td>Phone number</td>
          <td>Address</td>
          <td>City</td>
          <td>Country</td>
          <td colspan = 2>Actions</td>
        </tr>
    </thead>
    <tbody>
        @foreach($accounts as $account)
        <tr>
            <td>{{$account->id}}</td>
            <td>{{$account->name}} {{$account->surname}}</td>
            <td>{{$account->email}}</td>
            <td>{{$account->phone_number}}</td>
            <td>{{$account->address}}</td>
            <td>{{$account->city}}</td>
            <td>{{$account->country}}</td>
            <td>
                <a href="{{ route('accounts.edit',$account->id)}}" class="btn btn-primary">Edit</a>
            </td>
            <td>
                <form action="{{ route('accounts.destroy', $account->id)}}" method="post">
                  @csrf
                  @method('DELETE')
                  <button class="btn btn-danger" type="submit">Delete</button>
                </form>
            </td>
        </tr>
        @endforeach
    </tbody>
  </table>
<div>
<div class="col-sm-12">

  @if(session()->get('success'))
    <div class="alert alert-success">
      {{ session()->get('success') }}  
    </div>
  @endif
</div>
</div>
@endsection