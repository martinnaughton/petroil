<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Delivery extends Model
{
    protected $fillable = [
        'name_customer',
        'dimension',
        'type_oil',
        'oil_liter',
        'cost',
        'address',
        'phone_number'      
    ];
}
