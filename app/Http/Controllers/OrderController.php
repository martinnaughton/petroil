<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Order;
use Log;
use App\Account;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        Log::info('Order index');

        $orders = Order::all();

        return view('orders.index', compact('orders'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        Log::info('Order create');
        //account
        $orders = Order::find(3)->account;
        Log::info($orders);
        return view('orders.create', compact('orders'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        Log::info('Order store');

        Log::info($request);
        $request->validate([
            'dimension'=>'required',
            'type_oil'=>'required',
            'oil_liter'=>'required',
            'cost'=>'required',
            'delivery'=>'required'
        ]);

        $order = new Order([
            'dimension' => $request->get('dimension'),
            'type_oil' => $request->get('type_oil'),
            'oil_liter' => $request->get('oil_liter'),
            'cost' => $request->get('cost'),
            'delivery'=>$request->get('delivery')
        ]);
        $orders = Order::find(1)->orders;

        $order->save();
        return redirect('/orders')->with('success', 'Order saved!');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        Log::info('Order edit');

        $order = Order::find($id);
        return view('orders.edit', compact('order'));  
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        Log::info('Order update');

        $request->validate([
            'dimension'=>'required',
            'type_oil'=>'required',
            'oil_liter'=>'required',
            'cost'=>'required',
            'delivery'=>'required'
        ]);

        $order = Order::find($id);
        $order->dimension =  $request->get('dimension');
        $order->type_oil = $request->get('type_oil');
        $order->oil_liter = $request->get('oil_liter');
        $order->cost = $request->get('cost');
        $order->delivery = $request->get('delivery');
        $order->save();

        return redirect('/orders')->with('success', 'Order updated!');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Log::info('Order destroy');

        $order = Order::find($id);
        $order->delete();

        return redirect('/orders')->with('success', 'Order deleted!');
    }
}
